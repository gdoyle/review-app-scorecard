# Review App With Routes Demo

This is a demo of the GitLab [Review App](https://docs.gitlab.com/ee/ci/review_apps/) feaure with [Route Maps](https://docs.gitlab.com/ee/ci/review_apps/#route-maps).

## Repository Structure

This project requires a static site and a deployed environment in order to work.
This repository uses the following tools to achieve that.

### Gatsby

This project uses [Gatsby](https://www.gatsbyjs.com/) to build a static site. The current site is the default one
automatically generated when you initialize a new project.

### Surge.sh

In order to deploy review apps and a staging environment, this project is using [Surge.sh](https://surge.sh/) to easily host a static site.
